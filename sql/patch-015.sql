BEGIN;
-- Added support for comments for plans
ALTER TABLE plans add column comments TEXT;

CREATE OR REPLACE FUNCTION public.register_plan(in_title text, in_plan text, in_is_public boolean, in_is_anonymized boolean, in_username text, in_optimization_for text, in_query TEXT, in_comments TEXT)
    RETURNS register_plan_return
    LANGUAGE plpgsql
AS $function$
DECLARE
    use_hash_length int4 := 2;
    reply           register_plan_return;
BEGIN
    reply.delete_key := get_random_string( 50 );
    LOOP
        reply.id := get_random_string(use_hash_length);
        BEGIN
            INSERT INTO public.plans
                (  id,        title,     plan,     is_public,     entered_on,  is_anonymized,     delete_key,        added_by,     optimization_for,     query,     comments     )
            VALUES
                (  reply.id,  in_title,  in_plan,  in_is_public,  now(),       in_is_anonymized,  reply.delete_key,  in_username,  in_optimization_for,  in_query,  in_comments  );
            RETURN reply;
        EXCEPTION WHEN unique_violation THEN
            -- do nothing
        END;
        use_hash_length := use_hash_length + 1;
        IF use_hash_length >= 30 THEN
            raise exception 'Random string of length == 30 requested. Something''s wrong.';
        END IF;
    END LOOP;
END;
$function$;

commit;
