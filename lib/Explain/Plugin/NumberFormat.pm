package Explain::Plugin::NumberFormat;

use Number::Bytes::Human qw(format_bytes);
use Mojo::Base 'Mojolicious::Plugin';

sub register {
    my ( $self, $app ) = @_;

    # register helpers
    $app->helper( commify_number         => \&commify_number );
    $app->helper( commify_numbers_inside => \&commify_numbers_inside );
    $app->helper( humanize_size          => \&humanize_size );
}

sub commify_number {
    my $self = shift;

    # Code taken from perlfaq5
    local $_ = shift;
    return $_ unless defined $_;
    1 while s/^([-+]?\d+)(\d{3})/$1,$2/;
    return $_;
}

sub commify_numbers_inside {
    my $self = shift;
    my $msg  = shift;
    $msg =~ s{([:=]\s*)(\d{4,})}{$1 . $self->commify_number( $2 )}ge;
    return $msg;
}

sub humanize_size {
    my $self  = shift;
    my $nicer = format_bytes( shift ) . 'B';
    return 0 if $nicer eq '0B';
    $nicer =~ s/([A-Z]+)$/ $1/;
    return $nicer;
}

1;
