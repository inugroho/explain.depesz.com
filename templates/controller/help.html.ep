% layout 'default';

% my $title = 'Help';
% title $title;

<h1><%= $title =%></h1>

<p class="slogan"><strong>explain.depesz.com</strong> is a tool for finding real causes for slow queries.</p>

<p>Generally, one would use the <code>EXPLAIN <strong>ANALYZE</strong></code> query; and read the output. The problem is
that not all parts of the output are easily understandable by anybody, and it's not always obvious
whether a node that executes in 17.3ms is faster or slower than the one that runs in 100ms - given the
fact that the first one is executed 7 times.</p>

<p>To use the site, simply go to <a href="<%= url_for 'new-explain' %>" title="link to: new explain">first page</a>
and paste there explain analyze output from your psql.</p>

<p>After uploading, you will be directed to a page which shows parsed, and nicely (well, at least
nice for me :) colorized output, to emphasize important parts.</p>

<p>The url for colorized output is persistent, so you can easily show it to
others. For example to those nice guys on the irc channel <em>#postgresql</em> on freenode.</p>

<h2>Meaning of columns in <em>HTML</em> view</h2>

<p>While viewing <em>HTML</em> view, which is the default, you will be shown a table like this one:</p>

<img src="<%= url_for '/' %>img/help-4.png"/>

<p>In there you have following columns:</p>

<h3 id="col-no">Column: <em>#</em></h3>
<p>Simple node number for given node. The number itself is a link that you can share to someone to point to this specific part of explain.</p>

<h3 id="col-exclusive">Column: <em>exclusive</em></h3>
<p>How long did it take PostgreSQL to process this single node, but without time spent on any sub nodes. So, we can see that, in line 9, <em>Hash</em> used 4.1 seconds just do its work over rows provided by <em>WorkTable Scan</em> in line 10.</p>
<p>Please note that this number takes into account how many each process had to be run - This Hash was ran 5 times (value in loops column), so, each Harsh took only ~ 800ms. But in total, they took 4.1s.</p>
<p>Information about meaning of colors: <a href="#colors-exclusive">click here</a>.</p>

<h3 id="col-inclusive">Column: <em>inclusive</em></h3>
<p>This is similar to <em>exclusive</em> column described above, but this includes time spent by all sub nodes.</p>
<p>So, in our example, <em>inclusive</em> in <em>Hash</em> in line 9 is ~ 6.2 seconds, because it sums 4.1 second from <em>Hash</em> itself, and 2.1 seconds from <em>WorkTable Scan</em> in line 10.</p>
<p>Information about meaning of colors: <a href="#colors-inclusive">click here</a>.</p>

<h3 id="col-rows-x">Column: <em>rows x</em></h3>
<p>When planning how to run a query, PostgreSQLs is basing its decisions on statistics. You can see that it estimates some number of rows that will be returned for given node in <em>(cost=... rows=!!!</em> part of node description. For example, for <em>Hash</em> in line 9, it estimated that it will return 500 rows.</p>
<p>But sometimes/often the actual number is different. No stats are perfect. No assumptions are perfect. We can see the real number of rows returned in <em>(actual time=... rows=...</em> part, and also in <em>rows</em> column.</p>

<p>Difference between estimate and reality can be a problem. <em>Rows X</em> shows how many times the planner under- or over- estimated number of rows in given now.</p>

<p>Values with <em>&darr;</em> mean that planned underestimated by given number of times, and values with <em>&uarr;</em> would mean that it overestimated.</p>

<p>For example, <em>WorkTable Scan</em> in line 10 - PostgreSQL estimated that it will return 500 rows, but in reality it returnes almost 3 million rows. So <em>Rows X</em> shows <em>&darr; (2,949,677 / 500 ) ~~ 5899.4</em>.</p>
<p>Information about meaning of colors: <a href="#colors-rows-x">click here</a>.</p>

<h3 id="col-rows">Column: <em>rows</em></h3>
<p>How many rows this node actually returned. In case given node was run in many times, it's average returnes rows per loop.</p>

<p>In our example, <em>WorkTable Scan</em> in line 10 returnes, on average, 2,9 million rows, within 5 loops. So, in total, it returns ~ 14.7 million rows.</p>

<p>In another case, <em>Parallel Seq Scan</em> in line 8, reports that it returnes 1,788 million rows, within 40 loops. But this was with 8 concurrent workers, and in such case, rows shows number of rows (on average) per worker. So real, total, number of returned rows, is 8 * 1,788 million which is a bit over 14,3 million.</p>

<p>Sometimes you will get there also <em>"-" character, and a number</em>, for example like in lines 5 and 8, both being <em>Parallel Seq Scan</em>.</p>

<p>This information means that while processing this node, PostgreSQL got from sub-node, or from data some rows that it had to discard because of some kind of condition.</p>

<p>Usually you don't have to worry about it, but if you'd see <em>Seq Scan</em> which returns tiny fraction of removed rows - it might be good place to look for index.</p>
<p>Information about meaning of colors: <a href="#colors-rows">click here</a>.</p>

<h3 id="col-loops">Column: <em>loops</em></h3>
<p>This columns tells us how many times given node was run. For example, <em>WorkTable Scan</em> in line 10 was ran 10 times. And <em>Parallel Seq Scan</em> from line 8 was ran 40 times.</p>

<p>There is a bit more info here, though. We know (from <em>Gather</em> node in line 7) that the <em>Parallel Seq Scan</em> in line 8 was run in 8 concurrent workers. Which means that really, each worker only ran <em>40 / 8 = <b>5</b></em> loops.</p>

<p>This information is also present in <em>loops</em> column. Whenever you see there <em>number_a "/ character" number_b</em> it means that there were really <em>number_a</em> loops, but ran within <em>number_b</em> worker processes.</p>

<h3 id="col-read">Column: <em>read</em></h3>

<p>This is shown only if your explain used <em>buffers</em> option, and shows how many bytes were read from disk (or system disk cache) by given node.</p>

<h3 id="col-written">Column: <em>written</em></h3>

<p>This is shown only if your explain used <em>buffers</em> option, and shows how many bytes were written to disk by given node.</p>

<p>Please note that writes can also happen from plain read-only queries, as explained in <a href="https://www.depesz.com/2021/06/20/explaining-the-unexplainable-part-6-buffers/">this blogpost</a>.</p>

<h3 id="col-node">Column: <em>node</em></h3>

<p>This is textual representation of node, as close to original <em>EXPLAIN (ANALYZE ON, FORMAT TEXT)</em> format as possible.</p>

<h2>Interactive use</h2>

<p>While viewing explain in the <em>html</em> mode (the default one) you can switch to viewing source, reconstructed textual plan (if your plan was in json/yaml/xml format), and stats for the plan. To do so click on one of the tabs:</p>

<img src="<%= url_for '/' %>img/help-1.png"/>

<p>When hovering over plan, you might notice small stars, like this one: <img src="<%= url_for '/' %>img/star.png"/>. These mark nodes that are direct subnodes (children) of current node.</p>

<p>Additionally you can click on any node line, and it will fold all of its subnodes. For example, with plan like:</p>

<img src="<%= url_for '/' %>img/help-2.png"/>

<p>after you'd click on line 14, you'd get:</p>

<img src="<%= url_for '/' %>img/help-3.png"/>

<p>Clicking again on line 14 will unfold the lines.</p>

<p>Node types (for example: "<a href="http://www.depesz.com/2013/05/09/explaining-the-unexplainable-part-3/#hash-aggregate">HashAggregate</a>", "<a href="http://www.depesz.com/2013/04/27/explaining-the-unexplainable-part-2/#seq-scan">Seq Scan</a>" and others) are clickable, and will direct you to my description of this node type.</p>

<h2>Embedding on another pages</h2>

<p>It is possible to embed plan view on another page.</p>

<p>For example, let's assume you have <a href="https://explain.depesz.com/s/nMS5">a plan</a>. In its link you can see that the plan id is <strong>nMS5</strong>. Knowing this you can add to your page:</p>

<pre>
&lt;iframe width=800 height=200 src="https://explain.depesz.com/i/nMS5"&gt;&lt;/iframe&gt;
</pre>

<p>Which will give you, on your site:</p>

<iframe width=800 height=200 src="https://explain.depesz.com/i/nMS5"></iframe>

<h2>Reformatted query</h2>

<p>If you'd paste query, it will be displayed, using pasted format, in QUERY tab. But it will also be passed through <em>beautification</em> process, and shown in REFORMATTED QUERY tab.</p>

<p>Beautification is done using <a href="https://github.com/darold/pgFormatter/">pgFormatter</a> library.</p>

<h2>Colors</h2>

<p>This graph uses 4 colors to mark important things:</p>

<ul class="colors">
    <li class="c-1">white background - everything is fine</li>
    <li class="c-2">yellow background - given node is worrying</li>
    <li class="c-3">brown background - given node is more worrying</li>
    <li class="c-4">red background - given node is very worrying</li>
</ul>

<p>The color is chosen based on which mode you use: "<strong>Exclusive</strong>",
"<strong>Inclusive</strong>" or "<strong>Rows X</strong>".</p>

<h3 id="colors-exclusive">Colors for column <em>Exclusive</em></h3>

<p>This is the total amount of time <a href="http://www.postgresql.org" title="link to PostgreSQL site">PostgreSQL</a> spent
evaluating this node, without time spent in its subnodes. If the node has been executed many times (for example because
of a <code>Nested Loop</code> plan), this time will be correctly multiplied.</p>

<h4>Colors:</h4>

<ul class="colors">
    <li class="c-1">white background - is chosen if exclusive time &lt;= 10% of total query time</li>
    <li class="c-2">yellow background - is chosen if exclusive time &isin; (10%, 50%&gt; of total query time</li>
    <li class="c-3">brown background - is chosen if exclusive time &isin; (50%, 90%&gt; of total query time</li>
    <li class="c-4">red background - is chosen if exclusive time &gt; 90% of total query time</li>
</ul>

<h3 id="colors-inclusive">Colors for column <em>Inclusive</em></h3>

<p>This is just like <strong>Exclusive</strong>, but it doesn't exclude time of subnodes. So, by definition the top node will
have <strong>Inclusive</strong> time equal to the total time of the query.</p>

<h4>Colors:</h4>

<ul class="colors">
    <li class="c-1">white background - is chosen if inclusive time &lt;= 10% of total query time</li>
    <li class="c-2">yellow background - is chosen if inclusive time &isin; (10%, 50%&gt; of total query time</li>
    <li class="c-3">brown background - is chosen if inclusive time &isin; (50%, 90%&gt; of total query time</li>
    <li class="c-4">red background - is chosen if inclusive time &gt; 90% of total query time</li>
</ul>

<h3 id="colors-rows-x">Colors for column <em>Rows X</em></h3>

<p>This value stores information about how big the planner's mistake was when it estimated the return row count.</p>

<p>For example, if planner estimated that a given node will return 230 rows, but it returned 14118 rows, the
error is 14118/230 == 61.4.</p>

<p>It has to be noted that if the numbers were the other way around (estimated 14118, but really only 230), the
<strong>Rows X</strong> would be the same. To show whether planner underestimated or overestimated - there is
an arrow showing either &darr; - if planner underestimated rowcount, or &uarr; if it overestimated.</p>

<h4>Colors:</h4>

<ul class="colors">
    <li class="c-1">white background - is chosen if rows-x &lt;= 10</li>
    <li class="c-2">yellow background - is chosen if rows-x &isin; (10, 100&gt;</li>
    <li class="c-3">brown background - is chosen if rows-x &isin; (100, 1000&gt;</li>
    <li class="c-4">red background - is chosen if rows-x &gt; 1000</li>
</ul>

<h3 id="colors-rows">Colors for column <em>Rows</em></h3>

<p>This can be colored if there is LARGE number of removed rows.</p>

<p>For example, if there will be index scan on table, that will match 100 records, but additional (not-indexable) condition on the same table will remove 99 of these records, it means that you have 99% removal rate.</p>

<h4>Colors:</h4>

<ul class="colors">
    <li class="c-1">white background - is chosen if removal rate is &le;= 10%</li>
    <li class="c-2">yellow background - is chosen if removal rate &isin; (10%, 50%&gt;</li>
    <li class="c-3">brown background - is chosen if removal rate &isin; (50%, 90%&gt;</li>
    <li class="c-4">red background - is chosen if removal rate is &gt; 90%</li>
</ul>
